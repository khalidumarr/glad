-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for glad
DROP DATABASE IF EXISTS `glad`;
CREATE DATABASE IF NOT EXISTS `glad` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `glad`;


-- Dumping structure for table glad.gd_countries
DROP TABLE IF EXISTS `gd_countries`;
CREATE TABLE IF NOT EXISTS `gd_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

-- Dumping data for table glad.gd_countries: ~246 rows (approximately)
/*!40000 ALTER TABLE `gd_countries` DISABLE KEYS */;
REPLACE INTO `gd_countries` (`id`, `sortname`, `name`, `updated_at`, `created_at`) VALUES
	(1, 'AF', 'Afghanistan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(2, 'AL', 'Albania', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(3, 'DZ', 'Algeria', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(4, 'AS', 'American Samoa', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(5, 'AD', 'Andorra', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(6, 'AO', 'Angola', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(7, 'AI', 'Anguilla', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(8, 'AQ', 'Antarctica', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(9, 'AG', 'Antigua And Barbuda', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(10, 'AR', 'Argentina', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(11, 'AM', 'Armenia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(12, 'AW', 'Aruba', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(13, 'AU', 'Australia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(14, 'AT', 'Austria', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(15, 'AZ', 'Azerbaijan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(16, 'BS', 'Bahamas The', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(17, 'BH', 'Bahrain', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(18, 'BD', 'Bangladesh', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(19, 'BB', 'Barbados', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(20, 'BY', 'Belarus', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(21, 'BE', 'Belgium', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(22, 'BZ', 'Belize', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(23, 'BJ', 'Benin', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(24, 'BM', 'Bermuda', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(25, 'BT', 'Bhutan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(26, 'BO', 'Bolivia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(27, 'BA', 'Bosnia and Herzegovina', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(28, 'BW', 'Botswana', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(29, 'BV', 'Bouvet Island', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(30, 'BR', 'Brazil', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(31, 'IO', 'British Indian Ocean Territory', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(32, 'BN', 'Brunei', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(33, 'BG', 'Bulgaria', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(34, 'BF', 'Burkina Faso', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(35, 'BI', 'Burundi', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(36, 'KH', 'Cambodia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(37, 'CM', 'Cameroon', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(38, 'CA', 'Canada', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(39, 'CV', 'Cape Verde', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(40, 'KY', 'Cayman Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(41, 'CF', 'Central African Republic', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(42, 'TD', 'Chad', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(43, 'CL', 'Chile', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(44, 'CN', 'China', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(45, 'CX', 'Christmas Island', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(46, 'CC', 'Cocos (Keeling) Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(47, 'CO', 'Colombia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(48, 'KM', 'Comoros', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(49, 'CG', 'Congo', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(50, 'CD', 'Congo The Democratic Republic Of The', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(51, 'CK', 'Cook Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(52, 'CR', 'Costa Rica', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(54, 'HR', 'Croatia (Hrvatska)', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(55, 'CU', 'Cuba', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(56, 'CY', 'Cyprus', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(57, 'CZ', 'Czech Republic', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(58, 'DK', 'Denmark', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(59, 'DJ', 'Djibouti', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(60, 'DM', 'Dominica', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(61, 'DO', 'Dominican Republic', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(62, 'TP', 'East Timor', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(63, 'EC', 'Ecuador', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(64, 'EG', 'Egypt', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(65, 'SV', 'El Salvador', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(66, 'GQ', 'Equatorial Guinea', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(67, 'ER', 'Eritrea', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(68, 'EE', 'Estonia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(69, 'ET', 'Ethiopia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(70, 'XA', 'External Territories of Australia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(71, 'FK', 'Falkland Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(72, 'FO', 'Faroe Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(73, 'FJ', 'Fiji Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(74, 'FI', 'Finland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(75, 'FR', 'France', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(76, 'GF', 'French Guiana', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(77, 'PF', 'French Polynesia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(78, 'TF', 'French Southern Territories', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(79, 'GA', 'Gabon', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(80, 'GM', 'Gambia The', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(81, 'GE', 'Georgia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(82, 'DE', 'Germany', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(83, 'GH', 'Ghana', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(84, 'GI', 'Gibraltar', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(85, 'GR', 'Greece', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(86, 'GL', 'Greenland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(87, 'GD', 'Grenada', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(88, 'GP', 'Guadeloupe', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(89, 'GU', 'Guam', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(90, 'GT', 'Guatemala', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(91, 'XU', 'Guernsey and Alderney', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(92, 'GN', 'Guinea', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(93, 'GW', 'Guinea-Bissau', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(94, 'GY', 'Guyana', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(95, 'HT', 'Haiti', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(96, 'HM', 'Heard and McDonald Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(97, 'HN', 'Honduras', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(98, 'HK', 'Hong Kong S.A.R.', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(99, 'HU', 'Hungary', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(100, 'IS', 'Iceland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(101, 'IN', 'India', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(102, 'ID', 'Indonesia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(103, 'IR', 'Iran', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(104, 'IQ', 'Iraq', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(105, 'IE', 'Ireland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(106, 'IL', 'Israel', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(107, 'IT', 'Italy', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(108, 'JM', 'Jamaica', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(109, 'JP', 'Japan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(110, 'XJ', 'Jersey', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(111, 'JO', 'Jordan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(112, 'KZ', 'Kazakhstan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(113, 'KE', 'Kenya', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(114, 'KI', 'Kiribati', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(115, 'KP', 'Korea North', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(116, 'KR', 'Korea South', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(117, 'KW', 'Kuwait', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(118, 'KG', 'Kyrgyzstan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(119, 'LA', 'Laos', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(120, 'LV', 'Latvia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(121, 'LB', 'Lebanon', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(122, 'LS', 'Lesotho', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(123, 'LR', 'Liberia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(124, 'LY', 'Libya', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(125, 'LI', 'Liechtenstein', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(126, 'LT', 'Lithuania', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(127, 'LU', 'Luxembourg', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(128, 'MO', 'Macau S.A.R.', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(129, 'MK', 'Macedonia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(130, 'MG', 'Madagascar', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(131, 'MW', 'Malawi', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(132, 'MY', 'Malaysia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(133, 'MV', 'Maldives', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(134, 'ML', 'Mali', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(135, 'MT', 'Malta', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(136, 'XM', 'Man (Isle of)', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(137, 'MH', 'Marshall Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(138, 'MQ', 'Martinique', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(139, 'MR', 'Mauritania', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(140, 'MU', 'Mauritius', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(141, 'YT', 'Mayotte', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(142, 'MX', 'Mexico', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(143, 'FM', 'Micronesia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(144, 'MD', 'Moldova', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(145, 'MC', 'Monaco', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(146, 'MN', 'Mongolia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(147, 'MS', 'Montserrat', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(148, 'MA', 'Morocco', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(149, 'MZ', 'Mozambique', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(150, 'MM', 'Myanmar', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(151, 'NA', 'Namibia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(152, 'NR', 'Nauru', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(153, 'NP', 'Nepal', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(154, 'AN', 'Netherlands Antilles', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(155, 'NL', 'Netherlands The', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(156, 'NC', 'New Caledonia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(157, 'NZ', 'New Zealand', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(158, 'NI', 'Nicaragua', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(159, 'NE', 'Niger', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(160, 'NG', 'Nigeria', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(161, 'NU', 'Niue', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(162, 'NF', 'Norfolk Island', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(163, 'MP', 'Northern Mariana Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(164, 'NO', 'Norway', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(165, 'OM', 'Oman', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(166, 'PK', 'Pakistan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(167, 'PW', 'Palau', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(168, 'PS', 'Palestinian Territory Occupied', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(169, 'PA', 'Panama', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(170, 'PG', 'Papua new Guinea', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(171, 'PY', 'Paraguay', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(172, 'PE', 'Peru', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(173, 'PH', 'Philippines', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(174, 'PN', 'Pitcairn Island', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(175, 'PL', 'Poland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(176, 'PT', 'Portugal', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(177, 'PR', 'Puerto Rico', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(178, 'QA', 'Qatar', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(179, 'RE', 'Reunion', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(180, 'RO', 'Romania', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(181, 'RU', 'Russia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(182, 'RW', 'Rwanda', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(183, 'SH', 'Saint Helena', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(184, 'KN', 'Saint Kitts And Nevis', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(185, 'LC', 'Saint Lucia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(186, 'PM', 'Saint Pierre and Miquelon', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(187, 'VC', 'Saint Vincent And The Grenadines', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(188, 'WS', 'Samoa', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(189, 'SM', 'San Marino', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(190, 'ST', 'Sao Tome and Principe', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(191, 'SA', 'Saudi Arabia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(192, 'SN', 'Senegal', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(193, 'RS', 'Serbia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(194, 'SC', 'Seychelles', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(195, 'SL', 'Sierra Leone', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(196, 'SG', 'Singapore', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(197, 'SK', 'Slovakia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(198, 'SI', 'Slovenia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(199, 'XG', 'Smaller Territories of the UK', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(200, 'SB', 'Solomon Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(201, 'SO', 'Somalia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(202, 'ZA', 'South Africa', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(203, 'GS', 'South Georgia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(204, 'SS', 'South Sudan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(205, 'ES', 'Spain', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(206, 'LK', 'Sri Lanka', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(207, 'SD', 'Sudan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(208, 'SR', 'Suriname', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(209, 'SJ', 'Svalbard And Jan Mayen Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(210, 'SZ', 'Swaziland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(211, 'SE', 'Sweden', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(212, 'CH', 'Switzerland', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(213, 'SY', 'Syria', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(214, 'TW', 'Taiwan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(215, 'TJ', 'Tajikistan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(216, 'TZ', 'Tanzania', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(217, 'TH', 'Thailand', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(218, 'TG', 'Togo', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(219, 'TK', 'Tokelau', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(220, 'TO', 'Tonga', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(221, 'TT', 'Trinidad And Tobago', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(222, 'TN', 'Tunisia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(223, 'TR', 'Turkey', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(224, 'TM', 'Turkmenistan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(225, 'TC', 'Turks And Caicos Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(226, 'TV', 'Tuvalu', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(227, 'UG', 'Uganda', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(228, 'UA', 'Ukraine', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(229, 'AE', 'United Arab Emirates', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(230, 'GB', 'United Kingdom', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(231, 'US', 'United States', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(232, 'UM', 'United States Minor Outlying Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(233, 'UY', 'Uruguay', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(234, 'UZ', 'Uzbekistan', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(235, 'VU', 'Vanuatu', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(236, 'VA', 'Vatican City State (Holy See)', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(237, 'VE', 'Venezuela', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(238, 'VN', 'Vietnam', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(239, 'VG', 'Virgin Islands (British)', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(240, 'VI', 'Virgin Islands (US)', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(241, 'WF', 'Wallis And Futuna Islands', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(242, 'EH', 'Western Sahara', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(243, 'YE', 'Yemen', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(244, 'YU', 'Yugoslavia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(245, 'ZM', 'Zambia', '2016-08-13 21:36:13', '0000-00-00 00:00:00'),
	(246, 'ZW', 'Zimbabwe', '2016-08-13 17:37:52', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `gd_countries` ENABLE KEYS */;


-- Dumping structure for table glad.gd_dishes
DROP TABLE IF EXISTS `gd_dishes`;
CREATE TABLE IF NOT EXISTS `gd_dishes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `likes` int(10) unsigned DEFAULT '0',
  `photo_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table glad.gd_dishes: ~10 rows (approximately)
/*!40000 ALTER TABLE `gd_dishes` DISABLE KEYS */;
REPLACE INTO `gd_dishes` (`id`, `name`, `description`, `likes`, `photo_id`, `created_at`, `updated_at`) VALUES
	(18, 'Chicken Biryani', 'description is her', 2, 13, '2016-08-24 21:50:19', '2016-08-24 21:50:19'),
	(19, 'Chicken Biryani', 'description is her', 0, 14, '2016-08-23 22:03:25', NULL),
	(20, 'Chicken Biryani', 'description is her', 0, 15, '2016-08-23 22:03:57', NULL),
	(21, 'Chicken Biryani', 'description is her', 0, 16, '2016-08-23 22:04:03', NULL),
	(22, 'Chicken Biryani', 'description is her', 0, 17, '2016-08-23 22:05:52', NULL),
	(23, 'Chicken Biryani', 'description is her', 0, 18, '2016-08-24 22:02:01', NULL),
	(24, 'fghfghfgh', 'tyfrghfghfgh', 0, 19, '2016-08-24 22:06:13', NULL),
	(25, 'fghfghfg', 'fghfghfg', 0, 20, '2016-08-24 22:08:05', NULL),
	(26, 'Biryani Special', 'As usual, this doesn\'t really work across all browsers in the market (you know the usual suspects). Also as usual, the best way to know if the client supports this feature is object detection. I have updated the function we used in 2010, adding a check for the FormData interface:', 0, 21, '2016-08-24 22:36:15', NULL),
	(27, 'Biryani Special', 'As usual, this doesn\'t really work across all browsers in the market (you know the usual suspects). Also as usual, the best way to know if the client supports this feature is object detection. I have updated the function we used in 2010, adding a check for the FormData interface:', 0, 22, '2016-08-24 22:37:00', NULL);
/*!40000 ALTER TABLE `gd_dishes` ENABLE KEYS */;


-- Dumping structure for table glad.gd_uploads
DROP TABLE IF EXISTS `gd_uploads`;
CREATE TABLE IF NOT EXISTS `gd_uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `size` int(120) NOT NULL,
  `local_url` text,
  `global_url` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table glad.gd_uploads: ~10 rows (approximately)
/*!40000 ALTER TABLE `gd_uploads` DISABLE KEYS */;
REPLACE INTO `gd_uploads` (`id`, `name`, `size`, `local_url`, `global_url`, `created_at`, `updated_at`) VALUES
	(13, 'chicken-biryani-23634260.jpg', 224970, 'C:/xampp/htdocs/glad/public/uploads/chicken-biryani-23634260.jpg', 'public/uploads/chicken-biryani-23634260.jpg', '2016-08-23 22:02:45', NULL),
	(14, 'chicken-biryani-23634260.jpg', 224970, 'C:/xampp/htdocs/glad/public/uploads/chicken-biryani-23634260.jpg', 'public/uploads/chicken-biryani-23634260.jpg', '2016-08-23 22:03:25', NULL),
	(15, 'chicken-biryani-23634260.jpg', 224970, 'C:/xampp/htdocs/glad/public/uploads/chicken-biryani-23634260.jpg', 'public/uploads/chicken-biryani-23634260.jpg', '2016-08-23 22:03:57', NULL),
	(16, 'chicken-biryani-23634260.jpg', 224970, 'C:/xampp/htdocs/glad/public/uploads/chicken-biryani-23634260.jpg', 'public/uploads/chicken-biryani-23634260.jpg', '2016-08-23 22:04:03', NULL),
	(17, 'chicken-biryani-23634260.jpg', 224970, 'C:/xampp/htdocs/glad/public/uploads/chicken-biryani-23634260.jpg', 'public/uploads/chicken-biryani-23634260.jpg', '2016-08-23 22:05:52', NULL),
	(18, 'resized.JPG', 66369, 'C:/xampp/htdocs/glad/public/uploads/resized.JPG', 'public/uploads/resized.JPG', '2016-08-24 22:02:01', NULL),
	(19, 'spoon.png', 6781, 'C:/xampp/htdocs/glad/public/uploads/spoon.png', 'public/uploads/spoon.png', '2016-08-24 22:06:13', NULL),
	(20, 'spoon.png', 6781, 'C:/xampp/htdocs/glad/public/uploads/spoon.png', 'public/uploads/spoon.png', '2016-08-24 22:08:05', NULL),
	(21, 'spoon.png', 6781, 'C:/xampp/htdocs/glad/public/uploads/spoon.png', 'public/uploads/spoon.png', '2016-08-24 22:36:14', NULL),
	(22, 'spoon.png', 6781, 'C:/xampp/htdocs/glad/public/uploads/spoon.png', 'public/uploads/spoon.png', '2016-08-24 22:37:00', NULL);
/*!40000 ALTER TABLE `gd_uploads` ENABLE KEYS */;


-- Dumping structure for table glad.gd_users
DROP TABLE IF EXISTS `gd_users`;
CREATE TABLE IF NOT EXISTS `gd_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `email` varchar(120) DEFAULT NULL,
  `phone_number` varchar(16) DEFAULT NULL,
  `country_id` int(9) DEFAULT NULL,
  `age` int(9) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `password` int(9) DEFAULT NULL,
  `digest` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `FK_gd_users_gd_countries` (`country_id`),
  CONSTRAINT `FK_gd_users_gd_countries` FOREIGN KEY (`country_id`) REFERENCES `gd_countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table glad.gd_users: ~3 rows (approximately)
/*!40000 ALTER TABLE `gd_users` DISABLE KEYS */;
REPLACE INTO `gd_users` (`id`, `name`, `email`, `phone_number`, `country_id`, `age`, `gender`, `password`, `digest`, `created_at`, `updated_at`) VALUES
	(1, 'khalid umar', 'khalidumar03@gmail.com', '0526800224', 2, 2, 'male', 0, '123', '2016-08-24 21:14:16', '2016-08-24 21:14:16'),
	(6, 'khalid umar', 'khalidumar04@gmail.com', '0526800224', 2, 2, 'male', NULL, NULL, '2016-08-24 22:02:01', '2016-08-24 22:02:01'),
	(7, 'Usman Ali', 'mrusmanali95@gmail.com', '0558863814', 2, 26, 'male', NULL, NULL, '2016-08-24 22:37:00', '2016-08-24 22:37:00');
/*!40000 ALTER TABLE `gd_users` ENABLE KEYS */;


-- Dumping structure for table glad.gd_user_dishes
DROP TABLE IF EXISTS `gd_user_dishes`;
CREATE TABLE IF NOT EXISTS `gd_user_dishes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `dish_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gd_user_dishes_gd_users` (`user_id`),
  KEY `FK_gd_user_dishes_gd_dishes` (`dish_id`),
  CONSTRAINT `FK_gd_user_dishes_gd_dishes` FOREIGN KEY (`dish_id`) REFERENCES `gd_dishes` (`id`),
  CONSTRAINT `FK_gd_user_dishes_gd_users` FOREIGN KEY (`user_id`) REFERENCES `gd_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table glad.gd_user_dishes: ~10 rows (approximately)
/*!40000 ALTER TABLE `gd_user_dishes` DISABLE KEYS */;
REPLACE INTO `gd_user_dishes` (`id`, `user_id`, `dish_id`, `created_at`, `updated_at`) VALUES
	(18, 1, 18, '2016-08-23 22:02:45', NULL),
	(19, 1, 19, '2016-08-23 22:03:25', NULL),
	(20, 1, 20, '2016-08-23 22:03:57', NULL),
	(21, 6, 21, '2016-08-23 22:04:03', NULL),
	(22, 6, 22, '2016-08-23 22:05:52', NULL),
	(23, 6, 23, '2016-08-24 22:02:02', NULL),
	(24, 7, 24, '2016-08-24 22:06:13', NULL),
	(25, 7, 25, '2016-08-24 22:08:05', NULL),
	(26, 7, 26, '2016-08-24 22:36:15', NULL),
	(27, 7, 27, '2016-08-24 22:37:00', NULL);
/*!40000 ALTER TABLE `gd_user_dishes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
