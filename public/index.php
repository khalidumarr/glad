<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define( 'DS', '/' );
define('APP_PATH', realpath('..'));
define('APP_VER', '1.0.0');
define('JWT_KEY', 'gladisawesome');
define('JWT_EXP', 3600);
define('BASE_DIR', '/var/www/html/glad/');
define('UPLOADS_DIR', 'public/uploads/');

function show_data( $data ){
    echo '<pre>';
    var_dump( $data );
}

try {

    /**
     * Include composer autoload
     */    
    include "../vendor/autoload.php";


    $di = new FactoryDefault();

    /**
     * Include Services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Call the autoloader service.  We don't need to keep the results.
     */
    $di->getLoader();

    /**
     * Starting the application
     * Assign service locator to the application
     */
    $app = new Micro($di);

    /**
     * Include Application
     */
    include APP_PATH . '/app.php';

    /**
     * Handle the request
     */
    $app->handle();

} catch (\Exception $e) {
      echo $e->getMessage() . '<br>';
      echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
