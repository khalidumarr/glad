(function(){
	'use strict';

	angular
		.module('site')
		.directive('fileType', ['$parse', fileType]);

	function fileType($parse){
		var directive = {};
			directive.restrict = 'A';
			directive.scope = true;
			directive.link = link;

		function link(scope, element, attributes){
			var input = element[0];

			var getter = $parse(attributes.ngModel);
         	var setter = getter.assign;

			input.addEventListener('change', function(e){
				if(typeof input.files !== 'undefined')
					setter(scope.$parent, input.files[0]);
			});
		}
		return directive;
	}
})()