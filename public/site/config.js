(function(){
	'use strict';

	angular
		.module('site')
		.config([ '$routeProvider', '$locationProvider', config])
		.run(['$http', 'flashService', 'authService', run])
		.factory('appConstants',[appConstants]);

	function appConstants(){
        var factory             = {};
        factory.BaseUrl         = '//52.36.15.232/';
        factory.reqTrans 		= reqTrans;

        function reqTrans(obj){
        	var str = [];
	        for(var p in obj) str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	        return str.join("&");
        }
        return factory;
    }

    function run($http, flashService, authService){
    	//set request content type
		$http.defaults.headers.common['Content-type'] = 'application/x-www-form-urlencoded';

		// brigdeData.init();
		flashService.init();

		if(!authService.isGuest()){
			var user = authService.user();
			$http.defaults.headers.common['Authorization'] = user.token;
		}
    }

    function config($routeProvider, $locationProvider){
		$routeProvider	
			.when('/', {
				controller: 'indexController',
				templateUrl: 'site/views/index.html',
			})
			.when('/submit', {
				controller: 'submitController',
				templateUrl: 'site/views/submit.html',
			})
			.when('/vote', {
				controller: 'voteController',
				templateUrl: 'site/views/vote.html',
			});
		$locationProvider.html5Mode(true);
	}
})();