(function(){
	'use strict';

	angular
		.module('site', [
			'ngRoute',
			'ngCookies',
			'XhrModule',
			'FlashModule',
			'ParseModule',
			'AuthModule',
		]);


	Object.defineProperty(Object.prototype, 'fishout', {
		value: function(properties){
			if (this === undefined || this === null){ 
				return;
			}
			if (properties.length === 0){ 
				return this.valueOf();
			}

			if(properties[0].split(':').length == 2){
				var key = properties[0].split(':')[0];
				var element = properties[0].split(':')[1];
				var foundSoFar = this[key][element];
			}
			else{
				var foundSoFar = this[properties[0]];
			}
			//console.log(foundSoFar);
			var remainingProperties = properties.slice(1);
			if(typeof foundSoFar !== 'undefined' && foundSoFar !== null) return foundSoFar.fishout(remainingProperties);	
			else return;
		}
	});
})();