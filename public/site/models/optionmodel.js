(function(){
	'use strict';

	angular
		.module('site')
		.factory('optionsModel', ['$q', 'parseFactory', 'appConstants', 'flashService', 'xhrService', 'authService', optionsModel]);

	function optionsModel($q, parseFactory, appConstants, flashService, xhrService, authService){
		var factory = option;

		function option(){
			this.record = {}
			this.records = {}
			this.navigation = {}
			this.errors = {
				'has': {},
				'message': {}
			}
		}

		option.prototype.getOptions = function(){
			var self = this,
				url = 'glad/countries',
				deferred = $q.defer();

			xhrService.query(url, null, function(receive){
				var parse = new parseFactory(receive);
				if(parse.code === 200){
					self.record = parse.data;
					deferred.resolve(parse.message);
				}				
				else {
					deferred.reject(parse.message);
				}
			});

			return deferred.promise;
		}

		return factory;
	}
})()