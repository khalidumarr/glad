(function(){
	'use strict'

	angular
		.module('site')
		.factory('dishModel', ['$q', 'parseFactory', 'appConstants', 'flashService', 'xhrService', 'authService', dishModel]);

	function dishModel($q, parseFactory, appConstants, flashService, xhrService, authService){
		var factory = dish;

		function dish(){
			this.record = {}
			this.records = {}
			this.navigation = {}
			this.errors = {
				'has': {},
				'message': {}
			}
		}

		dish.prototype.addDish = function(){
			var self = this,
				url = 'glad/dish',
				deferred = $q.defer(),
				send = makeServerData(self.record);

			xhrService.postMultipart(url, send, function(receive){
				var parse = new parseFactory(receive);
				if(parse.code === 200){
					self.record = parse.data;
					flashService.success(parse.message);
					deferred.resolve(parse.message);
				}				
				else {
					self.errors = parse._errors(parse.errors);
					flashService.error(parse.message);
					deferred.reject(parse.message);
				}
			})

			function makeServerData(data){
				var formdata = new FormData();
					if(typeof data.cookage !== 'undefined') formdata.append('age', data.cookage);
					if(typeof data.cookemail !== 'undefined') formdata.append('email', data.cookemail);
					if(typeof data.cookgender !== 'undefined') formdata.append('gender', data.cookgender);
					if(typeof data.cookname !== 'undefined') formdata.append('name', data.cookname);
					if(typeof data.cookphone !== 'undefined') formdata.append('phone_number', data.cookphone);
					if(typeof data.dishname !== 'undefined') formdata.append('dish_name', data.dishname);
					if(typeof data.dishspecial !== 'undefined') formdata.append('dish_description', data.dishspecial);
					if(typeof data.cookcountry !== 'undefined') formdata.append('country_id', data.cookcountry);
					if(typeof data.dishimage !== 'undefined') formdata.append('dish_photo', data.dishimage);
				return formdata;
			}
			return deferred.promise;
		}

		dish.prototype.listDish = function(){
			var self = this,
				url = 'glad/dish',
				deferred = $q.defer(),
				params = makeServerData(self.params);

			xhrService.query(url, params, function(receive){
				var parse = new parseFactory(receive);
				if(parse.code === 200){
					self.records = parse.data.dishes;
					self.navigation = parse._navigation(parse.navigation);
					flashService.success(parse.message);
					deferred.resolve(parse.message);
				}
				else if(parse.code === 401){
					flashService.error(parse.message, false, true);
					authService.clearCredentials();
					authService.requiredLogin('/');
				}
				else {
					self.errors = parse._errors(parse.errors);
					flashService.error(parse.message);
					deferred.reject(parse.message);
				}
			});

			function makeServerData(data){
				return {
					'q': data.q,
					'per_page': 8,
					'page': data.page
				}
			}

			return deferred.promise;
		}

		dish.prototype.likeDish = function(id){
			var self = this,
				url = 'glad/dish/'+id+'/like',
				deferred = $q.defer();

			xhrService.put(url, null, function(receive){
				var parse = new parseFactory(receive);
				if(parse.code === 200){
					flashService.success(parse.message);
					deferred.resolve(parse.message);
				}
				else if(parse.code === 401){
					flashService.error(parse.message, false, true);
					authService.clearCredentials();
					authService.requiredLogin('/');
				}
				else {
					self.errors = parse._errors(parse.errors);
					flashService.error(parse.message);
					deferred.reject(parse.message);
				}
			});

			return deferred.promise;
		}

		return factory;
	}
})()