(function(){
	'use strict';

	angular
		.module('site')
		.factory('userModel', ['$q', '$timeout', 'parseFactory', 'flashService', 'xhrService', userModel]);

	function userModel($q, $timeout, parseFactory, flashService, xhrService){
		var factory = user;

		function user(){
			this.record = {}
			this.records = {}
			this.navigation = {}
			this.errors = {
				'has': {},
				'message': {}
			}
		}

		user.prototype.authenticate = function(){
			var self = this,
				url = 'glad/auth/login',
				deferred = $q.defer(),
				send = makeServerData(self.record);

			xhrService.post(url, send, function(receive){
				var parse = new parseFactory(receive);
				if(parse.code === 200){
					self.record = parse.data;
					deferred.resolve(parse.message);
				}
				else{
					self.errors = parse._errors(parse.errors);
					flashService.error(parse.message);
					deferred.reject(parse.message);
				}
			})

			function makeServerData(data){
				return {
					'email': data.username,
					'password': data.password
				}
			}
			return deferred.promise;
		}

		user.prototype.updatePassword = function(){
			var self = this,
				url = 'glad/auth/change-password',
				deferred = $q.defer(),
				send = makeServerData(self.record);

			xhrService.put(url, send, function(receive){
				var parse = new parseFactory(receive);
				if(parse.code === 200){
					self.record = parse.data;
					deferred.resolve(parse.message);
				}
				else{
					self.errors = parse._errors(parse.errors);
					flashService.error(parse.message);
					deferred.reject(parse.message);
				}
			})

			function makeServerData(data){
				return {
					'password': data.password
				}
			}
			return deferred.promise;
		}

		return factory;
	}
})()