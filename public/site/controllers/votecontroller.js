(function(){
	'user strict';

	angular
		.module('site')
		.controller('voteController', ['$scope', 'dishModel', 'authService', voteController]);

	function voteController($scope, dishModel, authService){
		if(authService.isGuest()) authService.requiredLogin('/');

		$scope.message = 'Vote';
		$scope.dishmodel = new dishModel();
		$scope.dishes = [];
		$scope.paginate = {};

		$scope.dishmodel.params = {'page': '1'}
		$scope.dishmodel.listDish().then(function(message){
			$scope.dishes = $scope.dishmodel.records;
			$scope.paginate = $scope.dishmodel.navigation;
		});

		$scope.likeDish = function(dish_id){
			$scope.dishmodel.likeDish(dish_id).then(function(message){
				$scope.dishmodel.listDish().then(function(message){
					$scope.dishes = $scope.dishmodel.records;
					$scope.paginate = $scope.dishmodel.navigation;
				});
			});
		}

		$scope.search = function(){
			$scope.dishmodel.params = {'page': '1', 'q': $scope.query}
			$scope.dishmodel.listDish().then(function(message){
				$scope.dishes = $scope.dishmodel.records;
				$scope.paginate = $scope.dishmodel.navigation;
			});
		}

		$scope.toPage = function(page){
			$scope.dishmodel.params = {'page': page}
			$scope.dishmodel.listDish().then(function(message){
				$scope.dishes = $scope.dishmodel.records;
				$scope.paginate = $scope.dishmodel.navigation;
			})
		}
	}
})()