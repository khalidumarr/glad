(function(){
	'user strict';

	angular
		.module('site')
		.controller('indexController', ['$scope', '$location', 'userModel', 'authService', indexController]);

	function indexController($scope, $location, userModel, authService){
		if(!authService.isGuest())$location.path('/vote');
		$scope.form = {}
		$scope.usermodel = new userModel();

		$scope.submit = function(){
			$scope.usermodel.record['username']  = $scope.username;
			$scope.usermodel.record['password']  = $scope.password;
			$scope.usermodel.authenticate().then(function(message){
				authService.setCredentials({'username': '', 'token': $scope.usermodel.record});
				$location.path('/vote');
			})
		}
	}
})()