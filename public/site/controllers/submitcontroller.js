(function(){
	'user strict';

	angular
		.module('site')
		.controller('submitController', ['$scope', '$location', 'dishModel', 'userModel', 'authService', 'optionsModel', submitController]);

	function submitController($scope, $location, dishModel, userModel, authService, optionsModel){
		$scope.form = {};
		$scope.newUser = false;
		$scope.dishmodel = new dishModel();
		$scope.usermodel = new userModel();
		$scope.optionsmodel  = new optionsModel();
		$scope.options = [];

		$scope.optionsmodel.getOptions().then(function(message){
			$scope.options = $scope.optionsmodel.record;
		});

		$scope.submit = function(){
			$scope.dishmodel.addDish().then(function(message){
				if($scope.dishmodel.record.new_user){
					$scope.newUser = true;
					authService.setCredentials({'username': '', 'token': $scope.dishmodel.record.token});					
				}
				else{
					$location.path('/vote');
				}
			});
		}

		$scope.submitPassword = function(){
			if(valid($scope.form.password)){
				$scope.usermodel.updatePassword().then(function(message){
					$scope.newUser = false;
					$location.path('/vote');
				});
			}
		}

		/* Client side validation */
		function valid(form){
			$scope.usermodel.errors = {'has':{},'message':{}};
			//required fields
			angular.forEach(form.$error.required, function(field){
				$scope.usermodel.errors.has[field.$name] = true;
				$scope.usermodel.errors.message[field.$name] = 'Field required';
			});

			//confirmPassword fields
			angular.forEach(form.$error.confirmPassword, function(field){
				$scope.usermodel.errors.has[field.$name] = true;
				$scope.usermodel.errors.message[field.$name] = 'Passwords dont match';
			});

			return !form.$invalid;
		}
	}
})()