(function(){
	'use strict';

	angular
		.module('ParseModule')
		.factory('parseFactory', [parseFactory]);

	function parseFactory(){
		var factory = parse;

		function parse(resp){
			this.code = resp.fishout(['data','status','code']);
            this.message = resp.fishout(['data','status','message']);
            this.data = resp.fishout(['data','output','data']);
            this.errors = resp.fishout(['data','output','errors']);
            this.records = resp.fishout(['data','output','data','records']);
            this.navigation = resp.fishout(['data','output','data','navigation']);
            
            this._errors = errors;
            this._navigation = navigation;

		}

		function errors(err){
			var object = {
                'has': {},
                'message': {}
            };
            if(typeof errors !== 'undefined'){
                angular.forEach(err, function(value, key){
                    object.has[value.field] = true;
                    object.message[value.field] = value.message;
                });
            }
            return object;
		}

		function navigation(nav){
			var object = {};
            if(typeof nav !== 'undefined'){
                object = {
                    'total_record': nav.total,
                    'page': nav.page,
                    'max_pages': nav.max_pages,
                    'per_page': nav.per_page
                };
            }
            return object;
		}

		return factory;
	}
})();