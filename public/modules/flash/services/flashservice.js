(function () {
	"use strict";

	angular
		.module('FlashModule')
		.service('flashService', ['$rootScope', '$timeout', 'parseFactory', flashService])

    function flashService($rootScope, $timeout, parseFactory) {
        var self = this;

        self.init = function() {
            $rootScope.$on('$locationChangeStart', function () {
                self.clear();
            });
        }

        self.clear = function() {
            var flash = $rootScope.flash;
            if (flash) {
                if (!flash.keepAfterLocationChange) {
                    delete $rootScope.flash;
                } else {
                    // only keep for a single location change
                    flash.keepAfterLocationChange = false;
                }
            }
        }

        self.success = function(response, showErrorList, keepAfterLocationChange) {
        	var parse = (typeof response === 'string') ? {'message': response} : new parseFactory(response);
            $rootScope.flash = {
                message: parse.message,
                list: (typeof showErrorList !== 'undefined') ? parse.errors : false,
                type: 'success', 
                keepAfterLocationChange: keepAfterLocationChange
            };
            $timeout(function(){
                delete $rootScope['flash']; 
            },7000);
        }

        self.error = function(response, showErrorList, keepAfterLocationChange) {
        	var parse = (typeof response === 'string') ? {'message': response} : new parseFactory(response);
            $rootScope.flash = {
                message: parse.message,
                list: (typeof showErrorList !== 'undefined') ? parse.errors : false,
                type: 'error',
                keepAfterLocationChange: keepAfterLocationChange
            };
            $timeout(function(){
                delete $rootScope['flash']; 
            },7000);
        }
    }
})();