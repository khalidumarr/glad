(function(){
	'use strict';

	angular
		.module('XhrModule')
		.service('xhrService', ['$http', 'appConstants', xhrService]);

	function xhrService($http, appConstants){
		var self = this;

		self.post = function (url, data, callback){
			$http({ 
				method: 'POST', 
				url: appConstants.BaseUrl+url,
				data : data,
			})
			.then(function(receive){
				callback(receive);
			})
			.catch(function(reason, cause){
				console.error(reason, cause);
			});
		}

		self.postMultipart = function(url, data, callback){
			var xhr = new XMLHttpRequest();
				xhr.open('POST', appConstants.BaseUrl+url);
				xhr.send(data);

				xhr.onreadystatechange = function(){
					if(xhr.readyState === 4 && xhr.status === 200){
						var object = JSON.parse(xhr.response);
						callback({'data': object})
					}
				}
		}

		self.put = function(url, data, callback){
			$http({ 
				method: 'PUT', 
				url: appConstants.BaseUrl+url,
				data : data,
			})
			.then(function(receive){
				callback(receive);
			})
			.catch(function(reason, cause){
				console.error(reason, cause);
			});
		}

		self.query = function(url, data, callback){
			$http({ 
				method: 'GET', 
				url: appConstants.BaseUrl+url,
				params : data,
			})
			.then(function(receive){
				callback(receive);
			})
			.catch(function(reason, cause){
				console.error(reason, cause);
			});
		}
	}
})()