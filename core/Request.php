<?php
namespace Glad;
class Request extends \Phalcon\Http\Request{
	public function getHeadParams( $key ){
		$head = getallheaders();
		if( $key && isset( $head[$key] ) ){
			return $head[$key];
		}

		if( $key ){
			return;
		}

		return $head;
	}
}