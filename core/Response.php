<?php
namespace Glad;
class Response extends \Phalcon\Http\Response{
	public function output( $data = array(), $code, $message = '', $errors = array() ){
		$this->setContentType('application/json', 'UTF-8');
		$content = array(
			'api'       => array(
				'version'   => APP_VER,
				'format'    => 'json'
			),
			'status'    => array(
				'code'      => $code,
				'message'   => ( $message ) ? $message : $this->getStatusByCode( $code )
			),
			'output'    => array(
				'data'      => ( $data ) ? $data : new \stdClass(),
				'errors'	=> $errors
			)
		);

		$this->setStatusCode( 200 );
		$this->setJsonContent( $content );
		$this->setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
		$this->setHeader('Access-Control-Allow-Headers', 'Authorization, Content-type, Access-Control-Allow-Origin'); 
		$this->setHeader('Access-Control-Allow-Origin', '*');
		$this->send();
		die();
	}

	protected function getStatusByCode( $code=200 ){
		$codes  = array(
			'200'   => 'OK',
			'201'   => 'Created',
			'304'   => 'Not Modified',
			'400'   => 'Bad Request',
			'401'   => 'Unauthorized',
			'403'   => 'Forbidden',
			'404'   => 'Not Found',
			'405'   => 'Method not allowed',
			'410'   => 'Gone',
			'415'   => 'Unsupported Media Type',
			'422'   => 'Unprocessable Entity',
			'429'   => 'Too Many Requests'
		);

		if( isset( $codes[$code] ) )
			return $codes[$code];
		else
			return $code;
	}
} 