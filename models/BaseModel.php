<?php
class BaseModel extends \Phalcon\Mvc\Model
{
	public function beforeSave(){
		$this->created_at = date( 'Y-m-d H:i:s', time() );
	}

	public function beforeUpdate(){
		$this->updated_at = date( 'Y-m-d H:i:s', time() );
	}
}
