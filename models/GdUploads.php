<?php

class GdUploads extends BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $id;

     /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $name;

     /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $size;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $local_url;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $global_url;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

     /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field size
     *
     * @param String $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Method to set the value of field local_url
     *
     * @param string $local_url
     * @return $this
     */
    public function setLocalUrl($local_url)
    {
        $this->local_url = $local_url;

        return $this;
    }

    /**
     * Method to set the value of field global_url
     *
     * @param string $global_url
     * @return $this
     */
    public function setGlobalUrl($global_url)
    {
        $this->global_url = $global_url;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field updated_at
     *
     * @param string $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field local_url
     *
     * @return string
     */
    public function getLocalUrl()
    {
        return $this->local_url;
    }

    /**
     * Returns the value of field global_url
     *
     * @return string
     */
    public function getGlobalUrl()
    {
        return $this->global_url;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field updated_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'gd_uploads';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GdUploads[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GdUploads
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
