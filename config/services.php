<?php

use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\Url as UrlResolver;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include __DIR__ . "/../config/config.php";
});

/**
 * Shared loader service
 */
$di->setShared('loader', function () use ( $di ) {
    $config = $di->getConfig();
    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    return $loader;
});

/**
 * Setting custom router in DI.
 */
$di->setShared('response', function(){
    /**
     * Include Autoloader
     */
    include APP_PATH . '/core/Response.php';

    return new Glad\Response();
});

/**
 * Setting custom router in DI.
 */
$di->setShared('request', function(){
    /**
     * Include Autoloader
     */
    include APP_PATH . '/core/Request.php';

    return new Glad\Request();
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($di) {
    $config = $di->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($di) {
    $config = $di->getConfig();

    $dbConfig = $config->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});
