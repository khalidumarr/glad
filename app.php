<?php

use \Firebase\JWT\JWT;

/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */


$app->before(function () use ($app) {	
	$method 	= $app->request->getMethod();
	$segments 	= explode( '/', $app->request->getURI() );
	$controller = $segments[2];
	$action 	= ( isset( $segments[3] ) ) ? $segments[3] : '';
	
	if( $method == 'POST' && $controller == 'auth' && $action == 'login' ){
		return true;
	}

	if( $method == 'POST' && $controller == 'dish' ){
		return true;
	}

	if( $method == 'GET' && $controller == 'countries' ){
		return true;
	}
	
	$request 	= $app->request->getJsonRawBody();
	$jwt 		= $app->request->getHeadParams( 'Authorization' );

	try{
		if( JWT::decode( $jwt, JWT_KEY, array('HS256') ) ){
			return true;			
		}
	}
	catch( \Exception $e ){
		$app->response->output( array(), 401, $e->getMessage(), null );
	}

    if ($app['session']->get('auth') == false) {

        $app['flashSession']->error("The user isn't authenticated");
        $app['response']->redirect("/error");

        // Return false stops the normal execution
        return false;
    }

    return true;
});

$app->post('/auth/login', function () use ($app) {
	$request 	= (object)$app->request->getJsonRawBody(); 
	$response 	= $app->response;

	if( isset( $request->email ) && isset( $request->password ) ){
		$user = GdUsers::findFirst( 'email = "' . $request->email . '" and password = "' . md5( $request->password ) . '"' );
		if( !$user ){
			$response->output( array(), 400, "Invalid Username or password", null );
		}

		$token 	= array(
		    "exp" 		=> time() + JWT_EXP,
		    "user_id"	=> $user->getId()
		);

		$jwt = JWT::encode($token, JWT_KEY);
		$response->output( $jwt, 200, "Successfully Logged in!", null );
	}
	else{
		$response->output( array(), 400, "Please Provide Username or password", null );
	}
});

$app->put('/auth/change-password', function () use ($app) {
	$request 	= $app->request->getJsonRawBody();
	$jwt 		= $app->request->getHeadParams( 'Authorization' );

	if( !isset( $request->password ) || strlen( $request->password ) < 6 || strlen( $request->password ) > 25 ){
		$app->response->output( array(), 400, "Password length should be between 6 and 25 chracters", null );
	}

	
	$data = JWT::decode( $jwt, JWT_KEY, array('HS256') );
	$user = GdUsers::findFirst( "id = $data->user_id" );
	$user->password = md5( $request->password );
	if( $user->save() ){
		$app->response->output( array(), 200, "Password Successfully Changed!", null );
	}

	$app->response->output( array(), 400, "Error Changing the password!", null );
});

$app->post('/dish', function () use ($app) {
	$request 	= (object) $app->request->getPost();
	$response 	= $app->response;
	$user 		= '';	

	$errors 	= array();
	$newUser 	= false;

	// starting transactions
	$app->db->begin();

	// validating & setting email
	if( !empty( $request->email ) && filter_var( $request->email, FILTER_VALIDATE_EMAIL ) ){
		$user = GdUsers::findFirst( "email = '$request->email'" );
		
		if( $user == false ){
			// create new user object and set flag
			$user 		= new GdUsers();
			$newUser 	= true;
			$user->setEmail( $request->email );
		}		
	}
	else{
		$errors[] = array( 'field' => 'email', 'message' => "Invalid Email Provided" );
	}

	// validating & setting phone
    if( !empty( $request->phone_number ) ){
    	if( strlen( $request->phone_number ) < 8 || strlen( $request->phone_number ) > 15 ){
    		$errors[] = array( 'field' => 'phone_number', 'message' => "invalid Phone Number" );
    	}

    	if( empty( !$user ) ){
    		$user->setPhoneNumber( $request->phone_number );    	
    	}
    }
    else{
    	$errors[] = array( 'field' => 'phone_number', 'message' => "Phone Number is Required" );
    }

    // validating & setting name
    if( !empty( $request->name ) ){
    	if( strlen( $request->name ) < 3 || strlen( $request->name ) > 50 ){
    		$errors[] = array( 'field' => 'name', 'message' => "invalid Name" );
    	}

    	if( empty( !$user ) ){
    		$user->setName( $request->name );    	
    	}
    }
    else{
		$errors[] = array( 'field' => 'name', 'message' => "Name is Required" );
    }

    // validating & setting country
    if( !empty( $request->country_id ) ){
    	if( GdCountries::findFirstById( $request->country_id ) === false ){
    		$errors[] = array( 'field' => 'country_id', 'message' => "invalid Country" );
    	}

    	if( empty( !$user ) ){
    		$user->setCountryId( $request->country_id );
    	}
    }
    else{
		$errors[] = array( 'field' => 'country_id', 'message' => "Country is Required" );
    }

    // validating & setting age
    if( !empty( $request->age ) ){
    	if( !is_numeric( $request->age ) ){
    		$errors[] = array( 'field' => 'age', 'message' => "invalid Age" );
    	}

    	if( empty( !$user ) ){
    		$user->setAge( $request->age );
    	}
    }
    else{
		$errors[] = array( 'field' => 'age', 'message' => "Age is Required" );
    }

    // flushing errors to buffer
    if( !empty( $errors ) ){
    	$response->output( array(), 400, "Oops! Something is wrong in User Details", $errors );
    }

    // setting remaining fields
    if( !empty( $request->gender ) ){
    	$user->setGender( $request->gender );    	
    }

    if( $user->save() ){
    	// user is successfully updated / created, now lets add dish
    	$dish = new GdDishes();

    	if( !empty( $request->dish_name ) ){
    		$dish->setName( $request->dish_name );    		
    	}
    	else{
    		$errors[] = array( 'field' => 'dish_name', 'message' => "Dish Name is Required" );
    	}

    	if( !empty( $request->dish_description ) ){
    		$dish->setDescription( $request->dish_description );    		
    	}
    	else{
    		$errors[] = array( 'field' => 'dish_description', 'message' => "Dish Description is Required" );
    	}

    	// uploading dish image
    	if( $app->request->hasFiles() == true ){

            // print the real file names and sizes
            foreach ($app->request->getUploadedFiles() as $file) {
                $upload 			= new GdUploads();              
                $upload->name 		= $file->getName();
                $upload->size 		= $file->getSize();
                $upload->local_url 	= BASE_DIR . UPLOADS_DIR . $file->getName();
                $upload->global_url	= UPLOADS_DIR . $file->getName();
                $upload->save();

                // move the file into the application
                $file->moveTo( BASE_DIR . UPLOADS_DIR . $file->getName() );

                // adding uploaded file
                $dish->setPhotoId( $upload->getId() );  
            }
    	}
    	else{
    		$error[] = array( 'field' => 'dish_image', 'message' => "Dish Image is Required" );
    	}

    	// flushing errors to buffer
	    if( !empty( $errors ) ){
	    	$app->db->rollback();
	    	$response->output( array(), 400, "Oops! Something is wrong in Dish Details", $errors );
	    }

    	if( $dish->save() ){
    		$userDishes = new GdUserDishes();
    		$userDishes->user_id = $user->getId();
    		$userDishes->dish_id = $dish->getId();

    		if( $userDishes->save() ){
    			$app->db->commit();
				$token 	= array(
				    "exp" 		=> time() + JWT_EXP,
				    "user_id"	=> $user->getId()
				);

				$jwt = JWT::encode($token, JWT_KEY);

    			$response->output( array( 'token' => $jwt, 'new_user' => $newUser ), 200, "Congrates! Your dish is Successfully Added!", $errors );
    		}
    	}
    }

    $app->db->rollback();
    $response->output( null, 400, "Opss! Unable to add new Dish" );
});

$app->get('/dish', function () use ($app) {
	$request 	= (object)$app->request->get(); 
	$response 	= $app->response;

	// preparing pagination data
	$per_page 	= ( isset( $request->per_page ) ) ? $request->per_page : 2;
	$page 		= ( isset( $request->page ) ) ? $request->page : 1;
	$offset		= ( $page - 1 ) * $per_page;

	// pagination filters
	$queryBuilder = new \Phalcon\Mvc\Model\Query\Builder();
	$queryBuilder->columns('d.id, d.likes, d.name, f.global_url');
	$queryBuilder->from('GdUserDishes');
	$queryBuilder->innerJoin('GdUsers', 'u.id = GdUserDishes.user_id', 'u');
	$queryBuilder->innerJoin('GdDishes', 'd.id = GdUserDishes.dish_id', 'd');
	$queryBuilder->LeftJoin('GdUploads', 'f.id = d.photo_id', 'f');
	if( isset( $request->q ) ){			
		$queryBuilder->where("d.name like '%" . $request->q . "%'");
	}

	$queryBuilder->orderBy('d.id DESC');

	// calculating count
	$count = count( $queryBuilder->getQuery()->execute() );

	$queryBuilder->limit($per_page);
	$queryBuilder->offset($offset);
	$dishes = $queryBuilder->getQuery()->execute();

	// preparing result
	foreach( $dishes as $keyDish => $valDish ){
		$dishesAr[] 	= $valDish->toArray();
	}

	$data = array(
		'navigation' => array(
			'total'		=> $count,
			'page'		=> (int) ( ( isset( $request->page ) ) ? $request->page : 1 ),
			'max_pages'	=> ceil( $count / $per_page ),
			'per_page'	=> (int) $per_page
		),
		'dishes'	 =>  $dishesAr
	);

	$response->output( $data, 200, "$count records found successfully!" );
});

$app->get('/countries', function() use ($app) {
	$counties = GdCountries::find();
	foreach( $counties as $keyCountry => $valCountry ){
		$countiesAr[] = $valCountry->toArray();
	}

	$app->response->output( $countiesAr, 200, "Countries Successfully Fetched!", null );
});

$app->put('/dish/{id}/like', function ( $id ) use ($app) {
	$request 	= $app->request->getJsonRawBody();
	$jwt 		= $app->request->getHeadParams( 'Authorization' );
	$data 		= JWT::decode( $jwt, JWT_KEY, array('HS256') );

	$dish 		= GdDishes::findFirst( "id = $id" );
	$dish->likes++;
	if( $dish->save() ){
		$app->response->output( array(), 200, "Like Accpted!", null );
	}

	$app->response->output( array(), 400, "Problem Liking Dish!", null );
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
	if( strpos( $app->request->getURI(), 'docs' ) !== false ){
		include "docs/index.html";
	}
    else{
    	include "public/site/index.html";
    }

    $app->response->setStatusCode( 200 ); 
	$app->response->setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
	$app->response->setHeader('Access-Control-Allow-Headers', 'Authorization, Content-type, Access-Control-Allow-Origin'); 
	$app->response->setHeader('Access-Control-Allow-Origin', '*');
	$app->response->send();
	die;
});
